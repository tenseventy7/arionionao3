import { TestBed } from '@angular/core/testing';

import { OverlayBackService } from './overlay-back.service';

describe('OverlayBackService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: OverlayBackService = TestBed.get(OverlayBackService);
    expect(service).toBeTruthy();
  });
});
