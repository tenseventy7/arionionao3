import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./home/tabs/tabs.module').then(m => m.TabsPageModule)
  },
  { path: 'option/about', loadChildren: './option/about/about.module#AboutPageModule' },
  { path: 'option/settings', loadChildren: './option/settings/settings.module#SettingsPageModule' },
  { path: 'option/feedback', loadChildren: './option/feedback/feedback.module#FeedbackPageModule' },
  { path: 'work/:id/reading', loadChildren: './work/reading/reading.module#ReadingPageModule' },
  { path: 'user/:id/details', loadChildren: './user/details/details.module#DetailsPageModule' },
  { path: 'user/:id/works', loadChildren: './my/works/works.module#WorksPageModule' },
  { path: 'user/profile', loadChildren: './my/profile/profile.module#ProfilePageModule' },
  { path: 'pm/inbox', loadChildren: './messaging/inbox/inbox.module#InboxPageModule' },
  { path: 'pm/contacts', loadChildren: './messaging/contacts/contacts.module#ContactsPageModule' },
  { path: 'pm/composer', loadChildren: './messaging/composer/composer.module#ComposerPageModule' }
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
